package org.fate7.sfgjms.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.fate7.sfgjms.config.JmsConfig;
import org.fate7.sfgjms.model.HelloWorldMessage;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class HelloSender {

    private final JmsTemplate jmsTemplate;
    private final ObjectMapper objectMapper;

    @Scheduled(fixedRate = 2000)
    public void sendMessage() {
        //System.out.println("I'm sending a message");

        HelloWorldMessage message = HelloWorldMessage.builder()
                .id(UUID.randomUUID())
                .message("hello world !")
                .build();
        jmsTemplate.convertAndSend(JmsConfig.QUEUE, message);

        //System.out.println("message sent !");
    }

    @Scheduled(fixedRate = 2000)
    public void sendMessage2() throws JMSException {
        //System.out.println("I'm sending a message");

        HelloWorldMessage message = HelloWorldMessage.builder()
                .id(UUID.randomUUID())
                .message("hello!")
                .build();
        Message messageReceived = jmsTemplate.sendAndReceive(JmsConfig.QUEUE2, session -> {
            try {
                Message textMessage = session.createTextMessage(objectMapper.writeValueAsString(message));
                textMessage.setStringProperty("_type", "org.fate7.sfgjms.model.HelloWorldMessage");
                System.out.println("sending the message !" + message);
                return textMessage;
            } catch (JsonProcessingException e) {
                throw new JMSException("boom");
            }
        });

        System.out.println("receiving the message : " + messageReceived.getBody(String.class));

        //System.out.println("message sent !");
    }
}
