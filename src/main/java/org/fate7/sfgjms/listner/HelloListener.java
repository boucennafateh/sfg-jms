package org.fate7.sfgjms.listner;

import lombok.RequiredArgsConstructor;
import org.fate7.sfgjms.config.JmsConfig;
import org.fate7.sfgjms.model.HelloWorldMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class HelloListener {

    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.QUEUE)
    public void listen(
            @Payload HelloWorldMessage helloWorldMessage,
            @Headers MessageHeaders headers,
            Message message
            ){
        //System.out.println("I got a message");
        //System.out.println(helloWorldMessage);
        return;
    }

    @JmsListener(destination = JmsConfig.QUEUE2)
    public void listen2(
            @Payload HelloWorldMessage helloWorldMessage,
            @Headers MessageHeaders headers,
            Message message
    ) throws JMSException {

        HelloWorldMessage worldMessage = HelloWorldMessage.builder()
                .id(UUID.randomUUID())
                .message("world!")
                .build();

        jmsTemplate.convertAndSend(message.getJMSReplyTo(), worldMessage);
    }


}
